from django.forms import ModelForm

from tasks.models import Note


class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ["description"]
