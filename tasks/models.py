from django.db import models
from projects.models import USER_MODEL
from projects.models import Project

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, related_name="tasks"
    )
    assignee = models.ForeignKey(
        USER_MODEL, models.SET_NULL, null=True, related_name="tasks"
    )

    def __str__(self):
        return self.name


class Note(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    description = models.TextField()
    tasks = models.ForeignKey(
        "Task", on_delete=models.CASCADE, related_name="notes"
    )
