from django.forms import ModelForm

from projects.models import Message


class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = ["content"]