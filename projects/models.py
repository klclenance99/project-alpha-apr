from django.db import models
from django.conf import settings
from datetime import datetime, timedelta
from django.utils import timezone

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    members = models.ManyToManyField(USER_MODEL, related_name="projects")

    def __str__(self):
        return self.name

    def is_finished(self):
        for task in self.tasks.all():
            if task.is_completed is False:
                return False
        return True


class Message(models.Model):
    creator = models.ForeignKey(
        USER_MODEL, on_delete=models.PROTECT, related_name="messages"
    )
    project = models.ForeignKey(
        "Project", on_delete=models.CASCADE, related_name="messages"
    )
    content = models.TextField()
    date = models.DateTimeField(auto_now_add=True)


class Poll(models.Model):
    Title = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)

    def is_open(self):
        if timezone.now() - timedelta(days=1) > self.date:
            return False
        return True

    def winner(self):
        count = max([choice.number_of_votes for choice in self.choices.all()])
        chosen = self.choices.objects.get(number_of_votes=count)
        return chosen

    def already_taken(self):
        list = []
        for choice in self.choices.all():
            for voter in choice.voters:
                list.append(voter)
        if self.request.user in list:
            return True
        return False


class Choice(models.Model):
    name = models.CharField(max_length=200)
    poll = models.ForeignKey(
        "Poll", on_delete=models.CASCADE, related_name="choices"
    )
    number_of_votes = models.IntegerField(default=0)
    voters = models.ManyToManyField(USER_MODEL, related_name="choices")

    def __str__(self):
        return self.name

    def add_vote(self):
        self.number_of_votes += 1
