from django.urls import path
from projects.views import ProjectListView
from projects.views import ProjectDetailView
from projects.views import ProjectCreateView

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:id>/", ProjectDetailView, name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
