from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView
from django.shortcuts import redirect, render
from projects.forms import MessageForm

from projects.models import Project
from tasks.models import Task
from django.contrib.auth.mixins import LoginRequiredMixin
from datetime import datetime, timedelta
from django.utils import timezone

# Create your views here.
class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "projects/list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        tasks_upcoming = []
        tasks_past_due = []
        tasks_due_today = []
        for task in self.request.user.tasks.all():
            if (
                timezone.now() < task.due_date
                and timezone.now() + timedelta(days=1) > task.due_date
            ):
                tasks_due_today.append(task)
            elif timezone.now() > task.due_date:
                tasks_past_due.append(task)
            elif timezone.now() + timedelta(days=7) > task.due_date:
                tasks_upcoming.append(task)
        context["tasks_upcoming"] = tasks_upcoming
        context["tasks_past_due"] = tasks_past_due
        context["tasks_due_today"] = tasks_due_today
        return context


def ProjectDetailView(request, id):
    if request.user.is_authenticated is False:
        return redirect("login")
    project = Project.objects.get(id=id)
    if request.method == "POST":
        form = MessageForm(request.POST)
        if form.is_valid():
            form.instance.project = project
            form.instance.creator = request.user
            form.save()
            return redirect("show_project", id)
    form = MessageForm()
    messages = project.messages.all()
    return render(
        request,
        "projects/detail.html",
        {"form": form, "messages": messages, "project": project},
    )


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "projects/create.html"
    fields = ["name", "description", "members"]

    def get_success_url(self):
        return reverse_lazy("show_project", args=(self.object.id,))
